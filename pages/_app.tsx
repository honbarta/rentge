import '../styles/globals.css'
import 'antd/dist/antd.css';
import type { AppProps } from 'next/app'
import Router from 'next/router';
import React from 'react';
import NProgress from 'nprogress'; //nprogress module
import 'nprogress/nprogress.css'; //styles of nprogress

import { PropertyProvider } from '../components/propertyContext';

Router.events.on('routeChangeStart', () => NProgress.start()); 
Router.events.on('routeChangeComplete', () => NProgress.done()); 
Router.events.on('routeChangeError', () => NProgress.done());  


function MyApp({ Component, pageProps }: AppProps) {

  return (
    <PropertyProvider>
      <Component {...pageProps} />
    </PropertyProvider>
  )
}
export default MyApp
