// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from 'next'

export default (req: NextApiRequest, res: NextApiResponse) => {
    if (req.method === 'POST') {
        const offer = req.body
        console.log('offer', offer)
        res.status(200).send({success: true})
    }
}
