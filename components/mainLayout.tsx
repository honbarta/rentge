import { Layout, Menu, Breadcrumb, Image, List, Avatar, Space } from "antd";
import Head from "next/head";
import React, { ReactElement } from "react";

import styles from '../styles/Home.module.css'

const { Header, Content, Footer } = Layout;

const MainLayout = ({ children }: { children: ReactElement }) => (
  <>
    <Head>
      <title>Rentge - renting good &amp; easy</title>
      <link rel="icon" href="/favicon.ico" />
    </Head>

    <Layout className="layout">
      <Header>
        <Menu theme="dark" mode="horizontal" defaultSelectedKeys={["1"]}>
          <Menu.Item key="1">Rentge</Menu.Item>
          <Menu.Item key="2">How it works</Menu.Item>
          <Menu.Item key="3">About</Menu.Item>
        </Menu>
      </Header>
      <Content>
        {/* <Breadcrumb style={{ margin: '16px 0' }}>
        <Breadcrumb.Item>Home</Breadcrumb.Item>
        <Breadcrumb.Item>List</Breadcrumb.Item>
        <Breadcrumb.Item>App</Breadcrumb.Item>
      </Breadcrumb> */}
        <div className="site-layout-content">
          {children}
        </div>
      </Content>
      <Footer style={{ textAlign: "center" }}>
        Rentge - Renting good and easy
      </Footer>
    </Layout>
  </>
);

export default MainLayout;
