import { createContext, ReactNode, useContext, useState } from "react";
import { Property } from "../data/srealityClient";

type propertyContextType = {
    property?: Property;
    setProperty: (property: Property) => void;
};

const propertyContextDefaultValues: propertyContextType = {
    property: undefined,
    setProperty: (property: Property) => {},
};

const PropertyContext = createContext<propertyContextType>(propertyContextDefaultValues);

export function useProperty() {
    return useContext(PropertyContext);
}

type Props = {
    children: ReactNode;
};

export function PropertyProvider({ children }: Props) {

    const [property, setProperty] = useState<Property>();

    const value: propertyContextType = {
        property,
        setProperty
    }

    return (
        <>
            <PropertyContext.Provider value={value}>
                {children}
            </PropertyContext.Provider>
        </>
    );
}