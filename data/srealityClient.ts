export type Dimensions = {
  lat: number,
  lon: number
}

export type Href = {
  href: string
}

export type Value = {
  name: string,
  value: string
}

export type Links = {
  self: Href,
  images: Href[],
  image_middle2: Href[]
}

export type Property = {
  hash_id: number,
  name: string,
  gps: Dimensions,
  isNew: boolean,
  locality: string,
  price: number,
  labelsAll: string[],
  _links: Links
}

export type PropertyDetail = {
  hash_id: number,
  meta_description: string,
  map: Dimensions,
  name: Value,
  locality: Value,
  text: Value,
  price_czk: Value,
  _embedded: {
    images: {
      _links: {
        self: Href,
        view: Href
      },
      id: number
    }[]
  }
}

export async function fetchProperties(): Promise<Property[]> {
  const response = await fetch('https://www.sreality.cz/api/cs/v2/estates?region_entity_type=municipality&region_entity_id=3468')
  const data = await response.json()
  return data._embedded.estates
}

export async function fetchProperty(propertyId: number): Promise<Property> {
  const response = await fetch(`https://www.sreality.cz/api/cs/v2/estates/${propertyId}`)
  const data = await response.json()
  return data
}
